// calculate factorial f(n); n<=34
fn factorial(n: u128) -> u128 {
    if n <= 1 {
        return 1;
    }
    return n * factorial(n - 1);
}

fn main() {
    println!("factorial(5) {}", factorial(5));
    println!("factorial(34) {}", factorial(34));
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn ut_factorial(){
        assert_eq!(factorial(1), 1);
        assert_ne!(factorial(1), 0);
        assert_eq!(factorial(5), 120);
    }

    #[test]
    fn ut_factorial_not_overflow(){
        for i in 6..=34 {
            assert!(factorial(i) > 0);
        }
    }

    #[test]
    #[should_panic]
    fn ut_factorial_overflow_panic(){
        for i in 34..=37 {
            println!("i({})",i);
            assert!(factorial(i) > 0);
        }
    }
}