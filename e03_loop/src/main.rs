fn main() {
    let mut n = 0;
    loop {
        n += 1;
        if n > 5 {
            break;
        }
        println!("Hello, world ({})!", n);
    }

    while n > 0 {
        n -= 1;
        println!("Hello, step {}", n)
    }

    for i in 1..=5 {
        println!("for loop {}", i)
    }
    println!("All done!");
}
