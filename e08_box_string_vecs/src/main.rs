// #[allow(dead_code)]
#[derive(Debug)]
pub struct LinkedList<T> {
    data: T,
    next: Option<Box<LinkedList<T>>>,
}

impl<T: std::ops::AddAssign> LinkedList<T> {
    pub fn add_up(&mut self, n: T) {
        self.data += n;
    }
}

fn main() {
    let mut ll = LinkedList {
        data: 3,
        next: Some(Box::new(LinkedList {
            data: 2,
            next: None,
        })),
    };
    if let Some(ref mut v) = ll.next {
        v.add_up(10);
    }

    let mut v = Vec::with_capacity(10);
    v.push("my team I".to_string());
    v.push("my team II".to_string());
    v.push("my team III".to_string());
    v.push("my team IV".to_string());
    v.push("my team V".to_string());
    v.push("my team VI".to_string());
    v.push("my team VII".to_string());
    v.push("my team VIII".to_string());
    v.push("my team IX".to_string());
    v.push("my team X".to_string());
    v.push("my team XI".to_string());
    println!("v.len = {}, capacity = {}", v.len(), v.capacity());

    println!("Hello, {:?}!", ll);
}
