pub struct Stepper {
    curr: i32,
    step: i32,
    max: i32,
}

impl Iterator for Stepper {
    type Item = i32;
    fn next(&mut self) -> Option<i32> {
        if self.curr >= self.max {
            return None;
        }
        let res = self.curr;
        self.curr += self.step;
        Some(res)
    }
}

fn main() {
    let mut st = Stepper {
        curr: 2,
        step: 3,
        max: 15,
    };
    loop {
        match st.next() {
            Some(i) => println!("loop {}", i),
            None => break,
        }
    }

    let mut st2 = Stepper {
        curr: 3,
        step: 4,
        max: 15,
    };
    while let Some(w) = st2.next() {
        println!("while st2 ({})", w);
    }

    let mut n = 1;
    while n <= 5 {
        println!("Hello, world({})!", n);
        n += 1;
    }

    let it = Stepper {
        curr: 5,
        step: 7,
        max: 47,
    };
    for i in it {
        println!("for {}", i);
    }
}
